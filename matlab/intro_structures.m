%% Structures in Matlab
% In Matlab structures are a special kind of array that can contain many
% different data types and sizes. This can be useful to group data that is related but
% not of the same type/size.
%%
% To create a structure use the "." to Define a field. For example:
myStruct.Names = {'Bob' 'Ben' 'Alex' 'John'};
myStruct.Grades = [67 58 89 85];
%%
% This creates a structure called "myStruct" with fields "Names" and
% "Grades". Accessing a particular entry just uses "()".
%%
aName = myStruct.Names(1)
aGrade = myStruct.Grades(1)
%%
% There are a number of built in Matlab functions which operate on
% structures.
%%
% This returns the field names of the structure.
fieldnames(myStruct)
%%
% This returns the values of a specific field.
getfield(myStruct,'Grades')
%%
% This checks whether the input is a field in the structure.
isfield(myStruct,'Names')
isfield(myStruct,'BLAH')
%% Notes
% To see the full list of functions that operate on Structures and for more
% examples please go to: https://www.mathworks.com/help/matlab/structures.html


