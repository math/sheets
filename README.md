HTML renders of workbooks.
====================================

This git repository is the home of the files served from
https://wiki.math.uwaterloo.ca/sheets

Please understand these worksheets can not be ensured to be AODA complient because:
 - data is submited from users not employees
 - the HTML render engins in matlab, jupyter etc.. are missing flexibility

If you find any issues not related to the content of the worksheets feel free to submit an issue report to:
https://git.uwaterloo.ca/math/sheets/issues


Adding new worksheets
-----------------------

If you wish to add or change worksheets please submit a pull request to:
https://git.uwaterloo.ca/math/sheets

