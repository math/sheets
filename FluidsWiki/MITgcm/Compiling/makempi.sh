#!/bin/bash

hostname=${HOSTNAME}

if [ ${HOSTNAME} == "hood" ]; then
    OPTFILE=hood_mpi_intel
    NP=40
elif [ `hostname | cut -b1-3` == "orc" ]; then
    OPTFILE=orca_mpi
    NP=20
    module load intel/12.1.3
    module load netcdf/intel/4.2
elif [ `hostname | cut -b1-3` == "gra" ]; then
    module load netcdf/4.4.1.1 netcdf-fortran/4.4.4 
    OPTFILE=graham_mpi
    NP=16
elif [ ${HOSTNAME} == "bow" ] || [ ${HOSTNAME} == "waterton" ] || [ ${HOSTNAME} == "minnewanka" ]; then
    module load mpt
    OPTFILE=bow_mpt
    NP=16
else
    echo "Unknown host"
    exit
fi

T1=`date +%s`

make clean
../../../tools/genmake2 -mods=../code -of ../../../tools/build_options/$OPTFILE -mpi
make depend


T2=`date +%s`

make -j $NP

T3=`date +%s`

echo "make-depend in $(($T2 - $T1)) seconds"
echo "compiled in $(($T3 - $T2)) seconds"

